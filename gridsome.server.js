/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const axios = require("axios");
const slugify = require("@sindresorhus/slugify");

module.exports = function (api) {
  api.loadSource(async (actions) => {
    const netlifyRedirects = actions.addCollection("NetlifyRedirects");

    async function getAssetUrl(packageType) {
      try {
        const results = await axios.get(
          "https://api.github.com/repos/ubports/ubports-installer/releases/latest"
        );
        return results.data.assets.find((asset) =>
          asset.name.toLowerCase().endsWith(packageType.toLowerCase())
        ).browser_download_url;
      } catch (error) {
        console.log(error);
        return "/";
      }
    }

    for (let el of ["exe", "deb", "dmg", "appimage"]) {
      netlifyRedirects.addNode({
        from: "/" + el,
        to: await getAssetUrl(el),
        status: 302
      });
    }

    netlifyRedirects.addNode({
      from: "/snap",
      to: "https://snapcraft.io/ubports-installer",
      status: 302
    });
  });
};
